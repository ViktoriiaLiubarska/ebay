package tests;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class ModifyCartTests extends BaseTest {

    private static final String EXPECTED_AMOUNT_OF_PRODUCTS_IN_CART = "2";

    @Test(priority = 6)
    public void checkAmountChangeInCart() {
        getHomePage().clickOnShopByCategoryButton();
        getHomePage().clickOnCellPhones();
        getBasePage().waitForPageLoadComplete(30);
        getCellPhonesSectionPage().clickOnXiaomiButton();
        getBasePage().waitForPageLoadComplete(30);
        getXiaomiPage().clickOnAddToCartButton();
        getBasePage().waitForPageLoadComplete(30);
        getShoppingCartPage().clickOnIncreaseAmountButton();
        getBasePage().waitVisibilityOfElement(30,
                getShoppingCartPage().getAmountOfProductsInCart(), EXPECTED_AMOUNT_OF_PRODUCTS_IN_CART);
        assertEquals(getShoppingCartPage().getTextOfAmountProductsInCart(), EXPECTED_AMOUNT_OF_PRODUCTS_IN_CART);
    }

    @Test(priority = 7)
    public void checkPriceChangeInCart() {
        getHomePage().clickOnShopByCategoryButton();
        getHomePage().clickOnCellPhones();
        getBasePage().waitForPageLoadComplete(30);
        getCellPhonesSectionPage().clickOnXiaomiButton();
        getBasePage().waitForPageLoadComplete(30);
        getXiaomiPage().clickOnAddToCartButton();
        getBasePage().waitForPageLoadComplete(30);
        String productPrice = getShoppingCartPage().getProductPrice();
        getShoppingCartPage().clickOnIncreaseAmountButton();
        getBasePage().waitVisibilityOfElement(30,
                getShoppingCartPage().getAmountOfProductsInCart(), EXPECTED_AMOUNT_OF_PRODUCTS_IN_CART);
        assertEquals(getShoppingCartPage().calculateProductPriceTotal(productPrice, 2),
                getShoppingCartPage().getProductPrice());
    }

    @Test(priority = 8)
    public void checkRemoveFromCart() {
        getHomePage().clickOnShopByCategoryButton();
        getHomePage().clickOnCellPhones();
        getBasePage().waitForPageLoadComplete(30);
        getCellPhonesSectionPage().clickOnXiaomiButton();
        getBasePage().waitForPageLoadComplete(30);
        getXiaomiPage().clickOnAddToCartButton();
        getBasePage().waitForPageLoadComplete(30);
        getShoppingCartPage().clickOnRemoveButton();
        getBasePage().waitVisibilityOfElement(30,
                getShoppingCartPage().GetEmptyCartMessage());
        assertTrue(getShoppingCartPage().GetEmptyCartMessage().isDisplayed());
    }
}