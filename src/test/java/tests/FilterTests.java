package tests;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class FilterTests extends BaseTest {

    private static final String FILTER_MODEL_VALUE = "iphone 6";
    private static final double FILTER_PRICE_VALUE = 150.00;

    @Test(priority = 3)
    public void checkFilterByModel() {
        getHomePage().clickOnShopByCategoryButton();
        getHomePage().clickOnCellPhones();
        getBasePage().waitForPageLoadComplete(30);
        getCellPhonesSectionPage().clickOnSmartphonesButton();
        getBasePage().waitVisibilityOfElement(60, getSmartphonesPage().getIphone6Checkbox());
        getSmartphonesPage().selectIphone6Checkbox();
        getBasePage().waitForPageLoadComplete(60);
        for (WebElement webElement : getSmartphonesPage().getFilterResultsProductsList()) {
            assertTrue(webElement.getText().toLowerCase().contains(FILTER_MODEL_VALUE));
        }
    }

    @Test(priority = 4)
    public void checkFilterByPrice() {
        getHomePage().clickOnShopByCategoryButton();
        getHomePage().clickOnCellPhones();
        getBasePage().waitForPageLoadComplete(30);
        getCellPhonesSectionPage().clickOnSmartphonesButton();
        getBasePage().waitVisibilityOfElement(60, getSmartphonesPage().getIphone6Checkbox());
        getSmartphonesPage().selectIphone6Checkbox();
        getBasePage().waitVisibilityOfElement(60, getSmartphonesPage().getUnder150$Checkbox());
        getSmartphonesPage().selectUnder150$Checkbox();
        getBasePage().waitForPageLoadComplete(60);
        for (WebElement webElement : getSmartphonesPage().getFilterResultsPricesList()) {
            assertTrue(getSmartphonesPage().getProductPrice(webElement) < FILTER_PRICE_VALUE);
        }
    }
}
