package tests;

import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class ValidationTests extends BaseTest {

    private static final String EMPTY_STRING = "";
    private static final String INCORRECT_EMAIL = "&& 45@ukr.net";
    private static final String UNREGISTERED_USERNAME = "daveglis";

    @Test(priority = 1)
    public void checkValidationForEmptyFieldInSignIn() {
        getHomePage().clickOnSignInButton();
        getBasePage().waitVisibilityOfElement(60, getSignInPage().signInButton());
        getSignInPage().enterUserCredentials(EMPTY_STRING);
        assertFalse(getSignInPage().signInButton().isEnabled());
    }

    @Test(priority = 2)
    public void checkValidationForInvalidDataInSignIn() {
        getHomePage().clickOnSignInButton();
        getBasePage().waitVisibilityOfElement(60, getSignInPage().signInButton());
        getSignInPage().enterUserCredentials(INCORRECT_EMAIL);
        assertTrue(getSignInPage().errorMessage().isDisplayed(), "No validation for incorrect e-mail");
        getSignInPage().enterUserCredentials(UNREGISTERED_USERNAME);
        assertTrue(getSignInPage().errorMessage().isDisplayed(), "No validation for unregistered username");
    }
}
