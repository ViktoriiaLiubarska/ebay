package tests;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class AddToCartTests extends BaseTest {

    private static final String EXPECTED_AMOUNT_OF_PRODUCTS_IN_CART = "1";

    @Test(priority = 5)
    public void checkAddToCart() {
        getHomePage().clickOnShopByCategoryButton();
        getHomePage().clickOnCellPhones();
        getBasePage().waitForPageLoadComplete(30);
        getCellPhonesSectionPage().clickOnXiaomiButton();
        getBasePage().waitForPageLoadComplete(30);
        getXiaomiPage().clickOnAddToCartButton();
        getBasePage().waitVisibilityOfElement(30, getShoppingCartPage().getAmountOfProductsInCart());
        assertEquals(getShoppingCartPage().getTextOfAmountProductsInCart(), EXPECTED_AMOUNT_OF_PRODUCTS_IN_CART);
    }
}
