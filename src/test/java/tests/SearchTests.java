package tests;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SearchTests extends BaseTest {

    private static final String SEARCH_KEYWORD = "huawei p30";

    @Test(priority = 1)
    public void checkThatSearchResultsContainsSearchWord() {
        getHomePage().searchByKeyword(SEARCH_KEYWORD);
        getBasePage().waitForPageLoadComplete(30);
        for (WebElement webElement : getSearchResultsPage().getSearchResultsProductsList()) {
            assertTrue(webElement.getText().toLowerCase().contains(SEARCH_KEYWORD));
        }
    }

    @Test(priority = 2)
    public void checkThatSearchResultsHaveChosenBuyingFormat() {
        getHomePage().searchByKeyword(SEARCH_KEYWORD);
        getBasePage().waitForPageLoadComplete(30);
        getSearchResultsPage().clickOnAuctionButton();
        assertEquals(getSearchResultsPage().getSearchResultsProductsList().size(),
                getSearchResultsPage().getSearchResultsBidsList().size());
    }
}
