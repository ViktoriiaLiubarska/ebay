package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ShoppingCartPage extends BasePage {

    @FindBy(xpath = "//i[@id='gh-cart-n']")
    private WebElement amountOfProductsInCart;

    @FindBy(xpath = "//div[contains(@class,'quantity-col')]//option[@value='2']")
    private WebElement increaseAmountButton;

    @FindBy(xpath = "//div[@class='item-price']//span[contains(text(),'$')]")
    private WebElement productPrice;

    @FindBy(xpath = "//button[@data-test-id='cart-remove-item']")
    private WebElement removeButton;

    @FindBy(xpath = "//span[contains(text(),'any items in your cart.')]")
    private WebElement emptyCartMessage;

    public ShoppingCartPage(WebDriver driver) {
        super(driver);
    }

    public String getTextOfAmountProductsInCart() {
        return amountOfProductsInCart.getText();
    }

    public WebElement getAmountOfProductsInCart() {
        return amountOfProductsInCart;
    }

    public void clickOnIncreaseAmountButton() {
        increaseAmountButton.click();
    }

    public String getProductPrice() {
        String price = productPrice.getText();
        return price.substring(price.indexOf('$') + 1);
    }

    public String calculateProductPriceTotal(String price, int quantity) {
        return String.valueOf(Double.parseDouble(price) * quantity);
    }

    public void clickOnRemoveButton() {
        removeButton.click();
    }

    public WebElement GetEmptyCartMessage() {
        return emptyCartMessage;
    }

}
