package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CellPhonesSectionPage extends BasePage {

    @FindBy(xpath = "//img[@alt='Cell Phones & Smartphones']")
    private WebElement smartphonesButton;

    @FindBy(xpath = "//div[@class='b-info__title '][contains(text(),'WCJ02ZM')]")
    private WebElement xiaomiButton;

    public CellPhonesSectionPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnSmartphonesButton() {
        smartphonesButton.click();
    }

    public void clickOnXiaomiButton() { xiaomiButton.click(); }
}
