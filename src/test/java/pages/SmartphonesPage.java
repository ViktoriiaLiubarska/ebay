package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SmartphonesPage extends BasePage {

    @FindBy(xpath = "//input[@aria-label='Apple iPhone 6']")
    private WebElement iphone6Checkbox;

    @FindBy(xpath = "//h3[@class='s-item__title']")
    private List<WebElement> filterResultsProductsListText;

    @FindBy(xpath = "//input[@aria-label='Under $150.00']")
    private WebElement under150$Checkbox;

    @FindBy(xpath = "//span[@class='s-item__price']")
    private List<WebElement> filterResultsPricesListText;

    public SmartphonesPage(WebDriver driver) {
        super(driver);
    }

    public void selectIphone6Checkbox() { iphone6Checkbox.click(); }

    public void selectUnder150$Checkbox() { under150$Checkbox.click(); }

    public WebElement getIphone6Checkbox() {
        return iphone6Checkbox;
    }

    public List<WebElement> getFilterResultsProductsList() {
        return filterResultsProductsListText;
    }

    public WebElement getUnder150$Checkbox() {
        return under150$Checkbox;
    }

    public List<WebElement> getFilterResultsPricesList() {
        return filterResultsPricesListText;
    }

    public double getProductPrice(WebElement element) {
        String price = element.getText();
        return Double.parseDouble(price.substring(price.indexOf('$') + 1, price.indexOf('.') + 3));
    }
}
