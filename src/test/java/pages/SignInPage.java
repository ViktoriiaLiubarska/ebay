package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPage extends BasePage {

    @FindBy(xpath = "//input[@id='userid']")
    private WebElement userCredentialsInput;

    @FindBy(xpath = "//button[@id='signin-continue-btn']")
    private WebElement signInButton;

    @FindBy(xpath = "//p[@id='signin-error-msg']")
    private WebElement errorMessage;

    public SignInPage(WebDriver driver) {
        super(driver);
    }

    public void enterUserCredentials(final String credentials) {
        userCredentialsInput.sendKeys(credentials, Keys.ENTER);
    }

    public WebElement signInButton() {
        return signInButton;
    }

    public WebElement errorMessage() {
        return errorMessage;
    }

}

