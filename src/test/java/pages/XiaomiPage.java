package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class XiaomiPage extends BasePage {

    @FindBy(xpath = "//a[@id='isCartBtn_btn']")
    private WebElement addToCartButton;

    public XiaomiPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnAddToCartButton() { addToCartButton.click(); }

}
