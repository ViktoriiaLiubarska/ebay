package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage{

    @FindBy(xpath = "//input[@class='gh-tb ui-autocomplete-input']")
    private WebElement searchInput;

    @FindBy(xpath = "//button[@id='gh-shop-a']")
    private WebElement shopByCategoryButton;

    @FindBy(xpath = "//a[@class='scnd'][contains(text(),'Cell phones')]")
    private WebElement cellPhonesButton;

    @FindBy(xpath = "//span[@id='gh-ug']//a[contains(@href,'signin')]")
    private WebElement signInButton;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void searchByKeyword(final String keyword) {
        searchInput.sendKeys(keyword, Keys.ENTER);
    }

    public void clickOnShopByCategoryButton() {
        shopByCategoryButton.click();
    }

    public void clickOnCellPhones() {
        cellPhonesButton.click();
    }

    public void clickOnSignInButton() { signInButton.click(); }

}
