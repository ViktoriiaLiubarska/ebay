package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchResultPage extends BasePage {

    @FindBy(xpath = "//h3[@class='s-item__title']")
    private List<WebElement> searchResultsProductsListText;

    @FindBy(xpath = "//h2[contains(text(),'Auction')]")
    private WebElement auctionButton;

    @FindBy(xpath = "//span[contains(@class,'s-item__bids')]")
    private List<WebElement> searchResultsBidsListText;

    public SearchResultPage(WebDriver driver) {
        super(driver);
    }

    public List<WebElement> getSearchResultsProductsList() {
        return searchResultsProductsListText;
    }

    public void clickOnAuctionButton() { auctionButton.click(); }

    public List<WebElement> getSearchResultsBidsList() {
        return searchResultsBidsListText;
    }

}
