#Task

Create Test Automation Framework for https://www.ebay.com/​

You should use: SeleniumWD, PageFactory (minimum 5 pages), TestNG​.

Implement 10 automation test scenarios that cover general features of the site. (try to write not only positive test cases but and negative too)​

Separate you tests for different classes by functionality (if it’s necessary)​.

Create testng.xml for your tests (if you have positive and negative tests, create two testng.xml – one for negative tests, one for positive)​.
